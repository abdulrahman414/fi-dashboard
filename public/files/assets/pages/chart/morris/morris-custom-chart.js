"use strict";
setTimeout(function(){
$(document).ready(function() {

    lineChart();


    $(window).on('resize',function() {
        window.lineChart.redraw();
    });
    
});

/*Line chart*/
function lineChart() {
    window.lineChart = Morris.Bar({
        element: 'line-example',
        data: [
            { y: '0-30', a: 5, b: 90 },
            { y: '30-60', a: 7, b: 65 },
            { y: '60-90', a: 9, b: 40 },
            { y: '90-120', a: 4, b: 65 },
            { y: '+120', a: 6, b: 65 },
        ],
        xkey: 'y',
        redraw: true,
        ykeys: ['a'],
        hideHover: 'auto',
        labels: ['Total'],
        barColors: ['#4680ff']
    });
}

/*Area chart*/
function areaChart() {
    window.areaChart = Morris.Area({
        element: 'area-example',
        data: [
            { y: '2006', a: 100, b: 90 },
            { y: '2007', a: 75, b: 65 },
            { y: '2008', a: 50, b: 40 },
            { y: '2009', a: 75, b: 65 },
            { y: '2010', a: 50, b: 40 },
            { y: '2011', a: 75, b: 65 },
            { y: '2012', a: 100, b: 90 }
        ],
        xkey: 'y',
        resize: true,
        redraw: true,
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B'],
        lineColors: ['#93EBDD', '#64DDBB']
    });
}

/*Donut chart*/
function donutChart() {
    window.areaChart = Morris.Donut({
        element: 'donut-example',
        redraw: true,
        data: [
            { label: "Download Sales", value: 2 },
            { label: "In-Store Sales", value: 50 },
            { label: "Mail-Order Sales", value: 20 }
        ],
        colors: ['#5FBEAA', '#34495E', '#FF9F55']
    });
}

// Morris bar chart

},350);
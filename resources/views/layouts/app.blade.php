<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ url('images/icon.png') }}">

    <title>@yield('title')</title>

    <!-- Bootstrap v4.0.0-beta -->
    <link rel="stylesheet" href="{{ url('assets/vendor_components/bootstrap/dist/css/bootstrap.css') }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('assets/vendor_components/font-awesome/css/font-awesome.css') }}">

    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ url('assets/vendor_components/Ionicons/css/ionicons.css') }}">

    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ url('assets/vendor_components/jvectormap/jquery-jvectormap.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('css/master_style.css') }}">

    <!-- Maximum Admin Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ url('css/skins/_all-skins.css') }}">
    @yield('custom-css')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a  class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">MAX</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">
                <img width="100" src="{{ url('images/logo.png') }}" />
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ url('images/users/'.Auth::user()->image) }}" class="user-image rounded-circle" alt="User Image">
                                <span class="d-none d-sm-inline-block"> {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</span>
                            </a>
                            <ul class="dropdown-menu scale-up">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="{{ url('images/users/'.Auth::user()->image) }}" class="img-fluid" alt="User Image">

                                    <p>
                                        {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}
                                        <small>Member since {{ Auth::user()->created_at }}</small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                             <!--    <li class="user-body">
                                    <div class="row">
                                        <div class="col text-center">
                                            <a href="#">Followers</a>
                                        </div>
                                        <div class="col text-center">
                                            <a href="#">Friends</a>
                                        </div>
                                        <div class="col text-center">
                                            <a href="#">Sales</a>
                                        </div>
                                    </div>
                                </li> -->
                                <!-- Menu Footer-->
                                <div class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{ route("edit_profile") }}" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </ul>
                        </li>
                        <!-- Messages: style can be found in dropdown.less-->
                        <!-- <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope"></i>
                                <span class="label label-success">5</span>
                            </a>
                            <ul class="dropdown-menu scale-up">
                                <li class="header">You have 5 messages</li>
                                <li>
                                    <ul class="menu inner-content-div">
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="{{ url('images/user2-160x160.jpg') }}" class="rounded-circle" alt="User Image">
                                                </div>
                                                <div class="mail-contnet">
                                                    <h4>
                                                        Lorem Ipsum
                                                        <small>
                                                            <i class="fa fa-clock-o"></i> 15 mins</small>
                                                    </h4>
                                                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="{{ url('images/user3-128x128.jpg') }}" class="rounded-circle" alt="User Image">
                                                </div>
                                                <div class="mail-contnet">
                                                    <h4>
                                                        Nullam tempor
                                                        <small>
                                                            <i class="fa fa-clock-o"></i> 4 hours</small>
                                                    </h4>
                                                    <span>Curabitur facilisis erat quis metus congue viverra.</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="{{ url('images/user4-128x128.jpg') }}" class="rounded-circle" alt="User Image">
                                                </div>
                                                <div class="mail-contnet">
                                                    <h4>
                                                        Proin venenatis
                                                        <small>
                                                            <i class="fa fa-clock-o"></i> Today</small>
                                                    </h4>
                                                    <span>Vestibulum nec ligula nec quam sodales rutrum sed luctus.</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="{{ url('images/user3-128x128.jpg') }}" class="rounded-circle" alt="User Image">
                                                </div>
                                                <div class="mail-contnet">
                                                    <h4>
                                                        Praesent suscipit
                                                        <small>
                                                            <i class="fa fa-clock-o"></i> Yesterday</small>
                                                    </h4>
                                                    <span>Curabitur quis risus aliquet, luctus arcu nec, venenatis neque.</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="{{ url('images/user4-128x128.jpg') }}" class="rounded-circle" alt="User Image">
                                                </div>
                                                <div class="mail-contnet">
                                                    <h4>
                                                        Donec tempor
                                                        <small>
                                                            <i class="fa fa-clock-o"></i> 2 days</small>
                                                    </h4>
                                                    <span>Praesent vitae tellus eget nibh lacinia pretium.</span>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="#">See all e-Mails</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell"></i>
                                <span class="label label-warning">7</span>
                            </a>
                            <ul class="dropdown-menu scale-up">
                                <li class="header">You have 7 notifications</li>
                                <li>
                                    <ul class="menu inner-content-div">
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users text-aqua"></i> Curabitur id eros quis nunc suscipit blandit.
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-warning text-yellow"></i> Duis malesuada justo eu sapien elementum, in semper diam posuere.
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users text-red"></i> Donec at nisi sit amet tortor commodo porttitor pretium a erat.
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-shopping-cart text-green"></i> In gravida mauris et nisi
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user text-red"></i> Praesent eu lacus in libero dictum fermentum.
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user text-red"></i> Nunc fringilla lorem
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user text-red"></i> Nullam euismod dolor ut quam interdum, at scelerisque ipsum imperdiet.
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="#">View all</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-flag"></i>
                                <span class="label label-danger">6</span>
                            </a>
                            <ul class="dropdown-menu scale-up">
                                <li class="header">You have 6 tasks</li>
                                <li>
                                    <ul class="menu inner-content-div">
                                        <li>
                                            <a href="#">
                                                <h3>
                                                    Lorem ipsum dolor sit amet
                                                    <small class="pull-right">30%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-aqua" style="width: 30%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">30% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                        <li>

                                            <a href="#">
                                                <h3>
                                                    Vestibulum nec ligula
                                                    <small class="pull-right">20%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-danger" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                        aria-valuemax="100">
                                                        <span class="sr-only">20% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                        <li>

                                            <a href="#">
                                                <h3>
                                                    Donec id leo ut ipsum
                                                    <small class="pull-right">70%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-light-blue" style="width: 70%" role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                        aria-valuemax="100">
                                                        <span class="sr-only">70% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        
                                        <li>
                                            <a href="#">
                                                <h3>
                                                    Praesent vitae tellus
                                                    <small class="pull-right">40%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-yellow" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                        aria-valuemax="100">
                                                        <span class="sr-only">40% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        
                                        <li>
                                            <a href="#">
                                                <h3>
                                                    Nam varius sapien
                                                    <small class="pull-right">80%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-red" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                        <span class="sr-only">80% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <h3>
                                                    Nunc fringilla
                                                    <small class="pull-right">90%</small>
                                                </h3>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-primary" style="width: 90%" role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                        aria-valuemax="100">
                                                        <span class="sr-only">90% Complete</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="#">View all tasks</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" data-toggle="control-sidebar">
                                <i class="fa fa-cog fa-spin"></i>
                            </a>
                        </li> -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="image">
                        <img src="{{ url('images/users/'.Auth::user()->image) }}" class="rounded-circle" alt="User Image">
                    </div>
                    <div class="info">
                        <p>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</p>
                        <a href="#">
                            <i class="fa fa-circle text-success"></i> Online</a>
                            <br/><br/>
                            <a href="{{ route('update_profile') }}">
                            <button type="button" class="btn btn-info rounded">Update Profile</button>
                            </a>
                    </div>
                </div>
                <!-- search form -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    @if (Auth::user()->privilege == 1)
                    <li class="active treeview">
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>Users</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ url('users/create') }}">
                                    <i class="fa fa-user-plus"></i>Add User</a>
                            </li>
                            <li class="active">
                                <a href="{{ url('users') }}">
                                    <i class="fa fa-users"></i> View All Users</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    <li class="treeview">
                        <a href="#">
                            <i class="fa   fa-file-text-o"></i>
                            <span>Posts Schedule</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ url('fb/PostsSchedule/Create') }}">
                                    <i class="fa fa-calendar-plus-o"></i> Add Post</a>
                            </li>
                            <li>
                                <a href="{{ url('fb/PostsSchedule') }}">
                                    <i class="fa fa-file-text-o"></i> View All Posts</a>
                            </li>
                      
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('fb_settings') }}">
                            <i class="fa fa-cogs"></i>
                            <span>Page Settings</span>
                            <span class="pull-right-container">
                            </span>
                        </a>
                        
                    </li>
                    
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-file-word-o"></i>
                            <span>Word Groups</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{route('words_group')}}">
                                    <i class="fa fa-plus-circlesdxddsdfsdfdd"></i>Add Word Group</a>
                            </li>
                            <li>
                                <a href="{{ route('all_words_groups') }}">
                                    <i class="fa fa-file-word-o"></i>View All Word Groups</a>
                            </li>
    
                        </ul>
                    </li>
                    
                </ul>
            </section>
            <!-- /.sidebar -->
            
        </aside>

        @yield('content')

        <footer class="main-footer">
            <div class="pull-right d-none d-sm-inline-block">
                <b>Version</b> 1.0
            </div>Copyright &copy; 2018
          EMA Bot. All Rights Reserved.
        </footer>

        <!-- Control Sidebar -->
        <!-- /.control-sidebar -->

        <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>

    </div>
    <!-- ./wrapper -->
    <script>
        var url = '{{ url('') }}';
    </script>
    <!-- jQuery 3 -->
    <script src="{{ url('assets/vendor_components/jquery/dist/jquery.js') }}"></script>

    <!-- popper -->
    <script src="{{ url('assets/vendor_components/popper/dist/popper.min.js') }}"></script>

    <!-- Bootstrap v4.0.0-beta -->
    <script src="{{ url('assets/vendor_components/bootstrap/dist/js/bootstrap.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ url('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
    @yield('custom-js')

    <!-- Maximum_admin App -->
    <script src="{{ url('js/template.js') }}"></script>



    <!-- Maximum_admin for demo purposes -->
    <script src="{{ url('js/demo.js') }}"></script>



</body>

</html>
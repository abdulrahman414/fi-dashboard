
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from ableproadmin.com/7.0/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 10 Sep 2018 00:08:12 GMT -->
<head>
    <title>Taqa Demo</title>
    <!-- HTML5 Shim and Respond.js') }} IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js') }} doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js') }}/1.4.2/respond.min.js') }}"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Able Pro Bootstrap admin template made using Bootstrap 4, it has tons of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
    <meta name="keywords" content="admin template, bootstrap admin template, bootstrap dashboard, admin theme, dashboard template, bootstrap dashboard template, bootstrap admin panel, dashboard theme, best admin template, dashboard theme, website templates, bootstrap 4 admin template">
    <meta name="author" content="Phoenixcoded" />
    <!-- Favicon icon -->
    <link rel="icon" href="http://ableproadmin.com/7.0/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{ url('files/bower_components/bootstrap/css/bootstrap.min.css') }}">
    <!-- waves.css') }} -->
    <link rel="stylesheet" href="{{ url('files/assets/pages/waves/css/waves.min.css') }}" type="text/css" media="all">
        <!-- morris chart -->
    <link rel="stylesheet" type="text/css" href="{{ url('files/bower_components/morris.js/css/morris.css') }}">

    <!-- feather icon -->
    <link rel="stylesheet" type="text/css" href="{{ url('files/assets/icon/feather/css/feather.css') }}">
    <!-- Style.css') }} -->
    <link rel="stylesheet" type="text/css" href="{{ url('files/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('files/assets/css/widget.css') }}">
</head>

<body>
    <!-- [ Pre-loader ] start -->
    <div class="loader-bg">
        <div class="loader-bar"></div>
    </div>
    <!-- [ Pre-loader ] end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <!-- [ Header ] start -->
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                        <i class="feather icon-toggle-right"></i>
                    </a>
                        <a href="index.html">
                    </a>
                        <a class="mobile-options waves-effect waves-light">
                        <i class="feather icon-more-horizontal"></i>
                    </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-prepend search-close">
										<i class="feather icon-x input-group-text"></i>
									</span>
                                        <input type="text" class="form-control" placeholder="Enter Keyword">
                                        <span class="input-group-append search-btn">
										<i class="feather icon-search input-group-text"></i>
									</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                <i class="full-screen feather icon-maximize"></i>
                            </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-bell"></i>
                                        <span class="badge bg-c-red">5</span>
                                    </div>
                                    <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <h6>Notifications</h6>
                                            <label class="label label-danger">New</label>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="img-radius" src="{{ url('files/assets/images/avatar-4.jpg') }}') }}" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Admin User</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="img-radius" src="{{ url('files/assets/images/avatar-3.jpg') }}') }}" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Joseph William</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="img-radius" src="{{ url('files/assets/images/avatar-4.jpg') }}" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Sara Soudein</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-message-square"></i>
                                        <span class="badge bg-c-green">3</span>
                                    </div>
                                </div>
                            </li>
                            <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="{{ url('files/assets/images/avatar-4.jpg') }}" class="img-radius" alt="User-Profile-Image">
                                        <span>Admin User</span>
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <a href="#!">
                                            <i class="feather icon-settings"></i> Settings
                                        </a>
                                        </li>
                                        <li>
                                            <a href="user-profile.html">
                                            <i class="feather icon-user"></i> Profile
                                        </a>
                                        </li>
                                        <li>
                                            <a href="email-inbox.html">
                                            <i class="feather icon-mail"></i> My Messages
                                        </a>
                                        </li>
                                        <li>
                                            <a href="auth-lock-screen.html">
                                            <i class="feather icon-lock"></i> Lock Screen
                                        </a>
                                        </li>
                                        <li>
                                            <a href="auth-normal-sign-in.html">
                                            <i class="feather icon-log-out"></i> Logout
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- [ Header ] end -->

            <!-- [ chat user list ] start -->
            <div id="sidebar" class="users p-chat-user showChat">
                <div class="had-container">
                    <div class="p-fixed users-main">
                        <div class="user-box">
                            <div class="chat-search-box">
                                <a class="back_friendlist">
                                <i class="feather icon-x"></i>
                            </a>
                                <div class="right-icon-control">
                                    <form class="form-material">
                                        <div class="form-group form-primary">
                                            <input type="text" name="footer-email" class="form-control" id="search-friends" required="">
                                            <span class="form-bar"></span>
                                            <label class="float-label">
                                            <i class="feather icon-search m-r-10"></i>Search Friend
                                        </label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="main-friend-list">
                                <div class="media userlist-box waves-effect waves-light" data-id="1" data-status="online" data-username="Josephin Doe">
                                    <a class="media-left" href="#!">
                                    <img class="media-object img-radius img-radius" src="{{ url('files/assets/images/avatar-3.jpg') }}" alt="Generic placeholder image ">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="chat-header">Josephin Doe</div>
                                    </div>
                                </div>
                                <div class="media userlist-box waves-effect waves-light" data-id="2" data-status="online" data-username="Lary Doe">
                                    <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="{{ url('files/assets/images/avatar-2.jpg') }}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Lary Doe</div>
                                    </div>
                                </div>
                                <div class="media userlist-box waves-effect waves-light" data-id="3" data-status="online" data-username="Alice">
                                    <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="{{ url('files/assets/images/avatar-4.jpg') }}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Alice</div>
                                    </div>
                                </div>
                                <div class="media userlist-box waves-effect waves-light" data-id="4" data-status="offline" data-username="Alia">
                                    <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="{{ url('files/assets/images/avatar-3.jpg') }}" alt="Generic placeholder image">
                                    <div class="live-status bg-default"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Alia<small class="d-block text-muted">10 min ago</small></div>
                                    </div>
                                </div>
                                <div class="media userlist-box waves-effect waves-light" data-id="5" data-status="offline" data-username="Suzen">
                                    <a class="media-left" href="#!">
                                    <img class="media-object img-radius" src="{{ url('files/assets/images/avatar-2.jpg') }}" alt="Generic placeholder image">
                                    <div class="live-status bg-default"></div>
                                </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Suzen<small class="d-block text-muted">15 min ago</small></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ chat user list ] end -->

            <!-- [ chat message ] start -->
            <div class="showChat_inner">
                <div class="media chat-inner-header">
                    <a class="back_chatBox">
                    <i class="feather icon-x"></i> Josephin Doe
                </a>
                </div>
                <div class="main-friend-chat">
                    <div class="media chat-messages">
                        <a class="media-left photo-table" href="#!">
                            <img class="media-object img-radius img-radius m-t-5" src="{{ url('files/assets/images/avatar-2.jpg') }}" alt="Generic placeholder image">
                        </a>
                        <div class="media-body chat-menu-content">
                            <div class="">
                                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                            </div>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                    <div class="media chat-messages">
                        <div class="media-body chat-menu-reply">
                            <div class="">
                                <p class="chat-cont">Ohh! very nice</p>
                            </div>
                            <p class="chat-time">8:22 a.m.</p>
                        </div>
                    </div>
                    <div class="media chat-messages">
                        <a class="media-left photo-table" href="#!">
                            <img class="media-object img-radius img-radius m-t-5" src="{{ url('files/assets/images/avatar-2.jpg') }}" alt="Generic placeholder image">
                        </a>
                        <div class="media-body chat-menu-content">
                            <div class="">
                                <p class="chat-cont">can you come with me?</p>
                            </div>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                </div>
                <div class="chat-reply-box">
                    <div class="right-icon-control">
                        <form class="form-material">
                            <div class="form-group form-primary">
                                <input type="text" name="footer-email" class="form-control" required="">
                                <span class="form-bar"></span>
                                <label class="float-label">
                                <i class="feather icon-search m-r-10"></i>Share Your Thoughts
                            </label>
                            </div>
                        </form>
                        <div class="form-icon ">
                            <button class="btn btn-success btn-icon  waves-effect waves-light">
                            <i class="feather icon-message-circle"></i>
                        </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ chat message ] end -->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <!-- [ navigation menu ] start -->
                    <nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-header">
                                    <img class="img-fluid" src="{{ url('files/assets/images/logo.png') }}" alt="Theme-Logo" style="width: auto" />

                                    <div class="user-details">
                                        <p id="more-details">Admin User<i class="feather icon-chevron-down m-l-10"></i></p>
                                    </div>
                                </div>
                                <div class="main-menu-content">
                                    <ul>
                                        <li class="more-details">
                                            <a href="user-profile.html">
                                            <i class="feather icon-user"></i>View Profile
                                        </a>
                                            <a href="#!">
                                            <i class="feather icon-settings"></i>Settings
                                        </a>
                                            <a href="auth-normal-sign-in.html">
                                            <i class="feather icon-log-out"></i>Logout
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pcoded-navigation-label">Navigation</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu active pcoded-trigger">
                                    <a href="javascript:void(0)" class="waves-effect waves-dark">
        								<span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                        <span class="pcoded-mtext">Dashboard</span>
                                    </a>

                                </li>
                            </ul>

                        </div>
                    </nav>
                    <!-- [ navigation menu ] end -->
                    <div class="pcoded-content">
                        <!-- [ breadcrumb ] start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h4 class="m-b-10">Dashboard</h4>
                                        </div>
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html">
                                                    <i class="feather icon-home"></i>
                                                </a>
                                            </li>
                                            <li class="breadcrumb-item">
                                                <a href="#!">Dashboard</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- [ breadcrumb ] end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <!-- [ page content ] start -->
                                        <div class="row">


                                            <div class="col-lg-12 col-md-12">
                                                <div class="card table-card">
                                                    <div class="card-header borderless ">
                                                        <h5>Vendors Open Items</h5>
                                                        <div class="card-header-right">
                                                            <ul class="list-unstyled card-option">
                                                                <li class="first-opt"><i class="feather icon-chevron-left open-card-option"></i></li>
                                                                <li><i class="feather icon-maximize full-card"></i></li>
                                                                <li><i class="feather icon-minus minimize-card"></i></li>
                                                                <li><i class="feather icon-refresh-cw reload-card"></i></li>
                                                                <li><i class="feather icon-trash close-card"></i></li>
                                                                <li><i class="feather icon-chevron-left open-card-option"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                        <div id="line-example"></div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- project and updates start -->
                                            <div class="col-lg-6 col-md-12">
                                                <div class="card table-card">
                                                    <div class="card-header borderless ">
                                                        <h5>Vendors Open Items</h5>
                                                        <div class="card-header-right">
                                                            <ul class="list-unstyled card-option">
                                                                <li class="first-opt"><i class="feather icon-chevron-left open-card-option"></i></li>
                                                                <li><i class="feather icon-maximize full-card"></i></li>
                                                                <li><i class="feather icon-minus minimize-card"></i></li>
                                                                <li><i class="feather icon-refresh-cw reload-card"></i></li>
                                                                <li><i class="feather icon-trash close-card"></i></li>
                                                                <li><i class="feather icon-chevron-left open-card-option"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                            <div id="customers-chart" style="min-height: 400px"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="card latest-update-card">
                                                    <div class="card-header borderless ">
                                                        <h5>Customers Open Items</h5>
                                                        <div class="card-header-right">
                                                            <ul class="list-unstyled card-option">
                                                                <li class="first-opt"><i class="feather icon-chevron-left open-card-option"></i></li>
                                                                <li><i class="feather icon-maximize full-card"></i></li>
                                                                <li><i class="feather icon-minus minimize-card"></i></li>
                                                                <li><i class="feather icon-refresh-cw reload-card"></i></li>
                                                                <li><i class="feather icon-trash close-card"></i></li>
                                                                <li><i class="feather icon-chevron-left open-card-option"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                        <div id="vendors-chart" style="min-height: 400px"></div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- project and updates end -->
                                            <!-- sale card start -->
                                            <div class="col-lg-3 col-md-6">
                                                <div class="card bg-c-blue total-card">
                                                    <div class="card-block">
                                                        <div class="text-left">
                                                            <h4>9</h4>
                                                            <p class="m-0">Total Customers</p>
                                                        </div>
                                                        <span class="label bg-c-blue value-badges">12%</span>
                                                    </div>
                                                    <div id="total-value-graph-1" style="height:100px;"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <div class="card bg-c-red total-card">
                                                    <div class="card-block">
                                                        <div class="text-left">
                                                            <h4>8</h4>
                                                            <p class="m-0">Total Vendors</p>
                                                        </div>
                                                        <span class="label bg-c-red value-badges">15%</span>
                                                    </div>
                                                    <div id="total-value-graph-2" style="height:100px;"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <div class="card bg-c-green total-card">
                                                    <div class="card-block">
                                                        <div class="text-left">
                                                            <h4>$5782</h4>
                                                            <p class="m-0">Debit Customers</p>
                                                        </div>
                                                        <span class="label bg-c-green value-badges">20%</span>
                                                    </div>
                                                    <div id="total-value-graph-3" style="height:100px;"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <div class="card bg-c-yellow total-card">
                                                    <div class="card-block">
                                                        <div class="text-left">
                                                            <h4>$3780</h4>
                                                            <p class="m-0">Most debit customers</p>
                                                        </div>
                                                        <span class="label bg-c-yellow value-badges">23%</span>
                                                    </div>
                                                    <div id="total-value-graph-4" style="height:100px;"></div>
                                                </div>
                                            </div>
                                            <!-- sale card end -->
                                            <!-- project and updates start -->
 
                                            <!-- project and updates end -->
                                        </div>
                                        <!-- [ page content ] end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ style Customizer ] start -->

                    <!-- [ style Customizer ] end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 10]>
    <div class="ie-warning">
        <h1>Warning!!</h1>
        <p>You are using an outdated version of Internet Explorer, please upgrade
            <br/>to any of the following web browsers to access this website.
        </p>
        <div class="iew-container">
            <ul class="iew-download">
                <li>
                    <a href="http://www.google.com/chrome/">
                        <img src="{{ url('files/assets/images/browser/chrome.png') }}" alt="Chrome">
                        <div>Chrome</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.mozilla.org/en-US/firefox/new/">
                        <img src="{{ url('files/assets/images/browser/firefox.png') }}" alt="Firefox">
                        <div>Firefox</div>
                    </a>
                </li>
                <li>
                    <a href="http://www.opera.com">
                        <img src="{{ url('files/assets/images/browser/opera.png') }}" alt="Opera">
                        <div>Opera</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.apple.com/safari/">
                        <img src="{{ url('files/assets/images/browser/safari.png') }}" alt="Safari">
                        <div>Safari</div>
                    </a>
                </li>
                <li>
                    <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                        <img src="{{ url('files/assets/images/browser/ie.png') }}" alt="">
                        <div>IE (9 & above)</div>
                    </a>
                </li>
            </ul>
        </div>
        <p>Sorry for the inconvenience!</p>
    </div>
<![endif]-->
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="{{ url('files/bower_components/jquery/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('files/bower_components/jquery-ui/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('files/bower_components/popper.js') }}/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('files/bower_components/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- waves js -->
    <script src="{{ url('files/assets/pages/waves/js/waves.min.js') }}"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="{{ url('files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js') }}"></script>
    <!-- Float Chart js -->
    <script src="{{ url('files/assets/pages/chart/float/jquery.flot.js') }}"></script>
    <script src="{{ url('files/assets/pages/chart/float/jquery.flot.categories.js') }}"></script>
    <script src="{{ url('files/assets/pages/chart/float/curvedLines.js') }}"></script>
    <script src="{{ url('files/assets/pages/chart/float/jquery.flot.tooltip.min.js') }}"></script>
    <!-- amchart js -->
    <script src="{{ url('files/assets/pages/widget/amchart/amcharts.js') }}"></script>
    <script src="{{ url('files/assets/pages/widget/amchart/serial.js') }}"></script>
    <script src="{{ url('files/assets/pages/widget/amchart/light.js') }}"></script>
    <!-- Morris Chart js -->
    <script src="{{ url('files/bower_components/raphael/js/raphael.min.js') }}"></script>
    <script src="{{ url('files/bower_components/morris.js/js/morris.js') }}"></script>

    <!-- Custom js -->
    <script src="{{ url('files/assets/pages/chart/morris/morris-custom-chart.js') }}"></script>

    <script src="{{ url('files/assets/js/pcoded.min.js') }}"></script>
    <script src="{{ url('files/assets/js/vertical/vertical-layout.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('files/assets/pages/dashboard/custom-dashboard.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('files/assets/js/script.min.js') }}"></script>
    <script src="https://cdn.zoomcharts-cloud.com/1/latest/zoomcharts.js"></script>
	<script>
    //"shkzg",
    var chart = new PieChart({
        container: document.getElementById("customers-chart"),
        data: {
            url: "{{ url('') }}/OpenItems/0005/K",
			autoCategories: ["blart", "konto"]
        },
        events: {
            onChartUpdate: function(event, args) {
            }
        }
    });
    var chart = new PieChart({
        container: document.getElementById("vendors-chart"),
        data: {
            url: "{{ url('') }}/OpenItems/0005/D",
			autoCategories: ["blart", "konto"]
        },
        events: {
            onChartUpdate: function(event, args) {
            }
        }
    });

	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-90716026-1');
	</script>


</body>

<!-- Mirrored from ableproadmin.com/7.0/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 10 Sep 2018 00:15:21 GMT -->
</html>

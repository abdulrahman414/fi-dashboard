@extends('layouts.app')
@section('title') All Words Groups @endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            All Words Groups
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> Home</a>
            </li>
  
            <li class="breadcrumb-item active">Words Groups</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    
        <!-- Validation wizard -->
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="card-title">View all words groups</h3>
    
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                    <table id="users" class="table table-bordered table-striped table-responsive">
                            <thead>
                                <tr>
                                <th>Page Name</th>
                                <th>Group Name</th>
                                <th>Delete</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                <th>Page Name</th>
                                <th>Group Name</th>
                                <th>Delete</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($wordGroups[0]->pages as $page)
                                @if(count($page->word_groups)>0)
                                <tr>
                                    <td>{{ $page->page_name }}</td>
                                    <td> @foreach ($page->word_groups as $group) 
                                            {{$group->group->name.' ,'}}<br/>
                                        @endforeach
                                    </td>
                          
                                    <td>
                                        <form  method="post" action="{{ route('delete_page_words_group',[$page->id]) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-block btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      

     
   </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('custom-css')
<!--alerts CSS -->
<link href="{{ url('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<!-- daterange picker -->

<link rel="stylesheet" href="{{ url('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">

@endsection

@section('custom-js')
<!-- Sweet-Alert  -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- This is data table -->
<script src="{{ url('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/pages/users/list.js')}}"></script>


@endsection

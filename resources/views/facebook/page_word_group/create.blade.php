@extends('layouts.app')
@section('title') Add Words Template to Page @endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Page Words Groups
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="breadcrumb-item active">Add Page Word Template</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success') }}
    </div>
    @endif
    <!-- Validation Forms -->
     <div class="box box-default">
       <div class="box-header with-border">
         <h3 class="card-title">Add Page Word Template</h3>
         

         <div class="box-tools pull-right">
           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
           <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
         </div>
       </div>
    
       <!-- /.box-header -->
       <div class="box-body">
         <div class="row">
           <div class="col">
             <form novalidate method="post" action="{{ route('save_page_words_groups') }}">
               
                @csrf
                @method('POST')


          
              <div class="form-group">
                <label>Words Groups</label>
                <select class="form-control select2" name="words_groups[]" multiple="multiple" data-placeholder="Select a word Group" style="width: 100%;">
                @foreach($wordGroups as $wordGroup)
                  <option @foreach($selectedWordGroups as $selected) {{($wordGroup->id==$selected->group_id) ? "selected" : ""}} @endforeach value="{{$wordGroup->id}}">{{ $wordGroup->name }}</option>
                  @endforeach
                </select>
              </div>
              <!-- /.form-group -->
              <input type="hidden" value="{{$id}}" name="page_id"/>

       
         <div class="text-xs-right">
           <button type="submit" class="btn btn-info">Submit</button>
         </div>
       </form>
             
           </div>
           <!-- /.col -->
         </div>
         <!-- /.row -->
       </div>
       <!-- /.box-body -->
     </div>
     <!-- /.box -->
     
   </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('custom-css')
 <!-- Select2 -->
 <link rel="stylesheet" href="{{ url('assets/vendor_components/select2/dist/css/select2.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ url('css/master_style.css')}}">
    
@endsection

@section('custom-js')
  
<!-- Select2 -->
<script src="{{ url('assets/vendor_components/select2/dist/js/select2.full.js') }}"></script>

<!-- FastClick -->
<script src="{{ url('assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ url('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

<!-- Form validator JavaScript -->
<script src="{{ url('js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    
    </script>
<!-- maximum_admin for advanced form element -->
<script src="{{ url('js/pages/advanced-form-element.js')}}"></script>
      



@endsection

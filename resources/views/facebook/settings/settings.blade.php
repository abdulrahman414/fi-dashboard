@extends('layouts.app')
@section('title') Facebook Page Settings @endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Facebook Settings
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="breadcrumb-item active">Facebook Settings</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    
    <!-- Validation Forms -->
     <div class="box box-default">
       <div class="box-header with-border">
         <h3 class="card-title">Settings</h3>
         

         <div class="box-tools pull-right">
           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
           <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
         </div>
       </div>
    
       <!-- /.box-header -->
       <div class="box-body">
         <div class="row">
           <div class="col">
             <form novalidate method="post" action="{{ route('save_page_settings') }}">
             {{method_field('PUT')}}
                @csrf
          <div class="form-group">
            <h5>Time Intervals between Posts <span class="text-danger">*</span></h5>
						<div class="controls">
              <input type="number" min="5" name="time_interval" value="{{$settings->time_interval}}" class="form-control" required data-validation-required-message="This field is required"> </div>
              <div class="form-control-feedback"><small>Minimun time is 5 Minutes</small></div>
          </div>
          

          <div class="form-group">
            <h5>Posts Count<span class="text-danger">*</span></h5>
						<div class="controls">
              <input type="number" min="1" max="15" name="posts_count" value="{{$settings->posts_count}}" class="form-control" required data-validation-required-message="This field is required"> </div>
              <div class="form-control-feedback"><small>Max Posts Scan to reply is 15 Posts</small></div>
					</div>
        <input type="hidden" name="page_id" value="{{$page_id}}"/>
    
         
         
        
          <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <h5>Schedule Repeating</h5>
              <div class="controls">
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" value="1"  name="repeat" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Repeat Posts Schedule</span> </label>
              </div>
            </div>
          </div>
          
        </div>
       
         <div class="text-xs-right">
           <button type="submit" class="btn btn-info">Submit</button>
         </div>
       </form>
             
           </div>
           <!-- /.col -->
         </div>
         <!-- /.row -->
       </div>
       <!-- /.box-body -->
     </div>
     <!-- /.box -->
     
   </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('custom-css')
    
@endsection

@section('custom-js')
<!-- FastClick -->
<script src="{{ url('assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ url('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

<!-- Form validator JavaScript -->
<script src="{{ url('js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>


@endsection

@extends('layouts.app') @section('title') Facebook Settings @endsection @section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Facebook Posts Schedule
    </h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{ route('home') }}">
          <i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li class="breadcrumb-item active">Posts Schedule</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    @if(session()->has('success'))
    <div class="alert alert-success">
      {{session()->get('success') }}
    </div>
    @endif
    <!-- Validation Forms -->
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="card-title">Choose Page</h3>


        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove">
            <i class="fa fa-remove"></i>
          </button>
        </div>
      </div>
      <form method="post" action="{{ url('fb/PostsSchedule/Store') }}" enctype="multipart/form-data">
      @csrf
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col">


              <div class="form-group">
                <label>Choose Page</label>
                <select name="page_id" class="form-control select2" style="width: 100%;">
                  @foreach($pages as $page)
                  <option value="{{$page->id}}">{{ $page->page_name }}</option>
                  @endforeach
                </select>
              </div>
              <!-- /.form-group -->


              <div class="text-xs-right">
                <button type="button" onclick="post_attributes_show(this)" class="btn btn-info">Next ></button>
              </div>
            

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- Basic Forms -->
    <div class="box box-default hide" id="post_attributes">
      <div class="box-header with-border">
        <h3 class="box-title">Post Attributes</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove">
            <i class="fa fa-remove"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-12">
            <div class="form-group row">
              <label for="example-text-input" class="col-sm-2 col-form-label">Post Type</label>
              <div class="col-sm-10">
                  <div class="demo-radio-button">
                      <input name="post_type" value="0" type="radio" id="radio_1" checked />
                      <label for="radio_1">Text</label>
                      <input name="post_type" value="1" type="radio" id="radio_2" />
                      <label for="radio_2">Photo</label>
                      <input name="post_type" value="2" type="radio" id="radio_3" />
                      <label for="radio_3">Video</label>
                    </div>
              </div>
            </div>
            <div class="form-group row hide" id="photo_input">
              <label for="example-search-input" class="col-sm-2 col-form-label">Photo</label>
              <div class="col-sm-10">
                <input class="form-control" name="image" type="file" >
              </div>
            </div>
            <div class="form-group row">
              <label for="example-search-input" class="col-sm-2 col-form-label">Text</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="text" style="margin-top: 0px; margin-bottom: 0px; height: 128px;"></textarea>
              </div>
            </div>
            <div class="text-xs-right">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </form>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection @section('custom-css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ url('assets/vendor_components/select2/dist/css/select2.min.css') }}">

<!-- Theme style -->
<link rel="stylesheet" href="{{ url('css/master_style.css')}}"> @endsection @section('custom-js')
<!-- Select2 -->
<script src="{{ url('assets/vendor_components/select2/dist/js/select2.full.js') }}"></script>

<!-- FastClick -->
<script src="{{ url('assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ url('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>


<!-- maximum_admin for advanced form element -->
<script src="{{ url('js/pages/advanced-form-element.js')}}"></script>
<script src="{{ url('js/pages/posts_schedule/form.js')}}"></script>
@endsection
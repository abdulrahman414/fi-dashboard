@extends('layouts.app')
@section('title') Add Words Group @endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Words Group
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="breadcrumb-item active">Add Words Group</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success') }}
    </div>
    @endif
    <!-- Validation Forms -->
     <div class="box box-default">
       <div class="box-header with-border">
         <h3 class="card-title">Add Words Group</h3>
         

         <div class="box-tools pull-right">
           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
           <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
         </div>
       </div>
    
       <!-- /.box-header -->
       <div class="box-body">
         <div class="row">
           <div class="col">
             <form novalidate method="post" action="{{ route('save_words_group') }}">
                @csrf
          <div class="form-group">
              <h5>Words Group Name <span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="text" name="name" class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>
          <div class="form-group">
              <h5>Words <span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="text"  name="words" data-role="tagsinput" class="form-control" required data-validation-required-message="This field is required"> </div>
                <div class="form-control-feedback"><small>Use <code>alt or move away</code>to add word</small></div>
          </div>
          
          <div class="form-group">
              <h5>Comments <span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="text" name="comments" data-role="tagsinput" class="form-control" required data-validation-required-message="This field is required"> </div>
                <div class="form-control-feedback"><small>Use <code>alt or move away</code>to add comment</small></div>
          </div>
          <div class="form-group">
              <h5>Messages <span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="text" name="messages" data-role="tagsinput" class="form-control" required data-validation-required-message="This field is required"> </div>
                <div class="form-control-feedback"><small>Use <code>alt or move away</code>to add message</small></div>
					</div>
          
    

       
         <div class="text-xs-right">
           <button type="submit" class="btn btn-info">Submit</button>
         </div>
       </form>
             
           </div>
           <!-- /.col -->
         </div>
         <!-- /.row -->
       </div>
       <!-- /.box-body -->
     </div>
     <!-- /.box -->
     
   </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('custom-css')
<link rel="stylesheet" href="{{ url('css/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ url('css/tags-inputs-app.css') }}">
    
@endsection

@section('custom-js')
<!-- FastClick -->
<script src="{{ url('assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ url('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

<!-- Form validator JavaScript -->
<script src="{{ url('js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
    </script>
<!-- Tags Inputs JavaScript -->
<script src="{{ url('js/pages/bootstrap-tagsinput.min.js') }}"></script>


@endsection

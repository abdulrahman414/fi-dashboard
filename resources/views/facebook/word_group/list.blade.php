@extends('layouts.app')
@section('title') All Words Groups @endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            All Pages which have groups of words
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> Home</a>
            </li>
  
            <li class="breadcrumb-item active">Words Groups</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success') }}
    </div>
    @endif
        <!-- Validation wizard -->
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="card-title">View all words groups</h3>
    
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                    <table id="users" class="table table-bordered table-striped table-responsive">
                            <thead>
                                <tr>
                                <th>Group Name</th>
                                <th>Words</th>
                                <th>Comments</th>
                                <th>Messages</th>
                                <th>Delete</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Group Name</th>
                                    <th>Words</th>
                                    <th>Comments</th>
                                    <th>Messages</th>
                                    <th>Delete</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($wordGroups as $wordGroup)
                                <tr>
                                    <td>{{ $wordGroup->name }}</td>
                                    <td> @foreach ($wordGroup->words as $word) 
                                            {{$word->word.' ,'}}<br/>
                                        @endforeach
                                    </td>
                                    <td> @foreach ($wordGroup->comments as $comment) 
                                            {{$comment->text.' ,'}} <br/>
                                        @endforeach
                                    </td>
                                    <td> @foreach ($wordGroup->messages as $message) 
                                            {{$message->text.' ,'}}<br/>
                                        @endforeach
                                    </td>
                                    <td>
                                        <form  method="post" action="{{ route('delete_words_group',[$wordGroup->id]) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-block btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      

     
   </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('custom-css')
<!--alerts CSS -->
<link href="{{ url('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<!-- daterange picker -->

<link rel="stylesheet" href="{{ url('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">

@endsection

@section('custom-js')
<!-- Sweet-Alert  -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- This is data table -->
<script src="{{ url('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/pages/users/list.js')}}"></script>


@endsection

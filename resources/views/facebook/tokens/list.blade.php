@extends('layouts.app')
@section('title') Tokens @endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            View Tokens
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ url('users') }}">
                    <i class="fa fa-user"></i> Tokens</a>
            </li>
            <li class="breadcrumb-item active">View</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Validation wizard -->
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="card-title">View User</h3>
               <a href="{{ url('users/add_token/'.$user_id) }}"><button type="button" class="btn btn-block btn-info">Add New</button></a>
    
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                    @csrf
                    <table id="tokens" class="table table-bordered table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>Token</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Token</th>
                                    <th>Delete</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($tokens as $token)
                                <tr>
                                    <td>{{ substr($token->token,0,100) }}</td>
                                    <td><a href="#" onclick="delete_token({{ $token->id }})"><button type="button" class="btn btn-block btn-danger">Delete</button></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      

     
   </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('custom-css')
<!--alerts CSS -->
<link href="{{ url('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<!-- daterange picker -->

<link rel="stylesheet" href="{{ url('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">

@endsection

@section('custom-js')
<!-- Sweet-Alert  -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- This is data table -->
<script src="{{ url('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/pages/tokens/list.js')}}"></script>


@endsection

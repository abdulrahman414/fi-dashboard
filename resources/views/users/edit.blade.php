@extends('layouts.app')
@section('title') Create New Users @endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Update User
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ url('users') }}">
                    <i class="fa fa-user"></i> Users</a>
            </li>
            <li class="breadcrumb-item active">Update</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Validation wizard -->
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="card-title">Update User</h3>
              <h6 class="card-subtitle">Please fill all required fields</h6>
    
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
                <form method="post" action="{{ route('update_profile') }}" class="validation-wizard wizard-circle" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    <!-- Step 1 -->
                    <h6></h6>
                    <section>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wfirstName2"> First Name : <span class="danger">*</span> </label>
                                <input type="text" class="form-control required" value="{{ $user->firstname }}" id="firstname" name="firstname"> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wlastName2"> Last Name : <span class="danger">*</span> </label>
                                    <input type="text" class="form-control required" value="{{ $user->lastname }}" id="lastname" name="lastname"> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wemailAddress2"> Email Address : <span class="danger">*</span> </label>
                                    <input type="email" class="form-control required"  value="{{ $user->email }}" id="email" name="email"> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wphoneNumber2">Phone Number :</label>
                                    <input type="tel" class="form-control" id="wphoneNumber2" value="{{ $user->phone }}" name="phone"> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wlocation2"> Password : <span class="danger">*</span> </label>
                                    <input type="password" class="form-control" id="password1" name="password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wdate2">Confirm Password :</label>
                                    <input type="password"  class="form-control" id="password2" > </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                        <label for="wdate2">Profile Photo :</label>
                                        <input type="file" name="image" id="exampleInputFile" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 2 -->


                </form>
    
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      

     
   </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('custom-css')
<!--alerts CSS -->
<link href="{{ url('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<!-- daterange picker -->

<link rel="stylesheet" href="{{ url('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">

@endsection

@section('custom-js')
<!-- FastClick -->
<script src="{{ url('assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>


<!-- steps  -->
<script src="{{ url('assets/vendor_components/jquery-steps-master/build/jquery.steps.js')}}"></script>
'
<!-- validate  -->
<script src="{{ url('assets/vendor_components/jquery-validation-1.17.0/dist/jquery.validate.min.js')}}"></script>
<!-- Sweet-Alert  -->
<script src="{{ url('assets/vendor_components/sweetalert/sweetalert.min.js')}}"></script>

<!-- Form validator JavaScript -->
<!-- date-range-picker -->
<script src="{{ url('assets/vendor_components/moment/min/moment.min.js') }}"></script>
<script src="{{ url('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ url('assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ url('js/pages/validation.js')}}"></script>


@endsection

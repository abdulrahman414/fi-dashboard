@extends('layouts.app')
@section('title') Create New Users @endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create New Users
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ url('users') }}">
                    <i class="fa fa-user"></i> Users</a>
            </li>
            <li class="breadcrumb-item active">Create</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Validation wizard -->
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="card-title">Create New User</h3>
              <h6 class="card-subtitle">Please fill all required fields</h6>
    
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body wizard-content">
                <form id="add_user_form" method="post" action="{{ url('users/store') }}" class="validation-wizard wizard-circle">
                    @csrf
                    <!-- Step 1 -->
                    <h6>Personal Data</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wfirstName2"> First Name : <span class="danger">*</span> </label>
                                    <input type="text" class="form-control required" id="firstname" name="firstname"> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wlastName2"> Last Name : <span class="danger">*</span> </label>
                                    <input type="text" class="form-control required" id="lastname" name="lastname"> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wemailAddress2"> Email Address : <span class="danger">*</span> </label>
                                    <input type="email" class="form-control required" id="email" name="email"> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wphoneNumber2">Phone Number :</label>
                                    <input type="tel" class="form-control" id="wphoneNumber2" name="phone"> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wlocation2"> Password : <span class="danger">*</span> </label>
                                    <input type="password" class="form-control" id="password1" name="password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wdate2">Confirm Password :</label>
                                    <input type="password" class="form-control" id="password2" > </div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 2 -->
                    <h6>Subscriptions</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="jobTitle2">Package Name :</label>
                                    <select class="form-control required" name="package_id">
                                        @foreach ($packages as $package)
                                            <option value="{{ $package->id }}">{{ $package->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="wfirstName2"> Start And End Date : <span class="danger">*</span> </label>
                                    <input type="text" class="form-control required" id="daterange" name="daterange"> </div>
                            </div>

                        </div>
                    </section>                   

                    <!-- Step 3 -->
                    <h6>Invoice</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="wintType1">Amount :</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">EGP</span>
                                        <input type="text" name="amount" class="form-control">
                                        <span class="input-group-addon">.00</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="wintType1">Description :</label>
                                    <textarea class="form-control" name="invoice_description"></textarea>
                                </div>
                            </div>
                        </div>
                    </section>

                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      

     
   </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('custom-css')
<!--alerts CSS -->
<link href="{{ url('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<!-- daterange picker -->

<link rel="stylesheet" href="{{ url('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">

@endsection

@section('custom-js')
<!-- FastClick -->
<script src="{{ url('assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>


<!-- steps  -->
<script src="{{ url('assets/vendor_components/jquery-steps-master/build/jquery.steps.js')}}"></script>
'
<!-- validate  -->
<script src="{{ url('assets/vendor_components/jquery-validation-1.17.0/dist/jquery.validate.min.js')}}"></script>
<!-- Sweet-Alert  -->
<script src="{{ url('assets/vendor_components/sweetalert/sweetalert.min.js')}}"></script>

<!-- Form validator JavaScript -->
<!-- date-range-picker -->
<script src="{{ url('assets/vendor_components/moment/min/moment.min.js') }}"></script>
<script src="{{ url('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ url('assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ url('js/pages/users/form.js')}}"></script>
<script src="{{ url('js/pages/validation.js')}}"></script>


@endsection

@extends('layouts.app')
@section('title') Create New Users @endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            View Users
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ url('users') }}">
                    <i class="fa fa-user"></i> Users</a>
            </li>
            <li class="breadcrumb-item active">View</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Validation wizard -->
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="card-title">View User</h3>
              <h6 class="card-subtitle">Please fill all required fields</h6>
    
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                    @csrf
                    <table id="users" class="table table-bordered table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Package</th>
                                    <th>Tokens</th>
                                    <th>View</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Privilege</th>
                                    <th>Tokens</th>
                                    <th>View</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ (!$user->privilege)?"User":"Admin" }}</td>
                                    <td><a href="{{ url('users/token/'.$user->id) }}"><button type="button" class="btn btn-block btn-info">Tokens ({{ count($user->tokens) }})</button></a></td>
                                    <td><a href="{{ url('users/show/'.$user->id) }}"><button type="button" class="btn btn-block btn-info">View</button></a></td>
                                    <td><a href="{{ url('users/edit/'.$user->id) }}"><button type="button" class="btn btn-block btn-success">Edit</button></a></td>
                                    <td><a href="#" onclick="delete_user({{ $user->id }})"><button type="button" class="btn btn-block btn-danger">Delete</button></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      

     
   </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('custom-css')
<!--alerts CSS -->
<link href="{{ url('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<!-- daterange picker -->

<link rel="stylesheet" href="{{ url('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">

@endsection

@section('custom-js')
<!-- Sweet-Alert  -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- This is data table -->
<script src="{{ url('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/pages/users/list.js')}}"></script>


@endsection

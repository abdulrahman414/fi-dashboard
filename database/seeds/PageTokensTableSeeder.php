<?php

use Illuminate\Database\Seeder;
use App\User;
use App\facebook\Page;
use App\facebook\Token;
use App\facebook\PageTokens;
use App\facebook\Settings;

class PageTokensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users=User::where('privilege',0)->get();
        foreach($users as $user){
            $page=Page::create([
                "fb_page_id" => mt_rand(),
                "page_name" => "Test Page",
                "user_id" => $user->id
            ]);
            $token=Token::create([
                "token" => mt_rand(),
                "user_id" => $user->id
            ]);
            PageTokens::create([
                'token_id' => $token->id,
                'page_id' => $page->id
            ]);
            Settings::create([
                "page_id"=>$page->id
            ]);
        }
    }
}

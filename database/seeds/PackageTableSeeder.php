<?php

use Illuminate\Database\Seeder;
use App\Package;
use App\Site;

class PackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $sites = Site::all();
        foreach ($sites as $site) {
            Package::create([
                'name' => 'Plan 1',
                'description' => 'plan 1 for test',
                'pages_count' => 5,
                'site_id' => $site->id
            ]);
        }
    }
}

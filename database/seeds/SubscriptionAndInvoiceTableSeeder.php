<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Invoice;
use App\Package;
use App\Subscription;

class SubscriptionAndInvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users = User::where('privilege', 0)->get();
        $package_id = Package::firstOrFail()->id;
        foreach ($users as $user) {
            $invoice = Invoice::create([
                'user_id' => $user->id,
                'amount' => rand(100, 1000),
            ]);
            Subscription::create([
                'user_id' => $user->id,
                'from' => '2018-01-01',
                'to' => '2018-12-30',
                'invoice_id'=>$invoice->id,
                'package_id'=>$package_id
            ]);
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin User
        User::create([
            'firstname' => 'Admin',
            'lastname' => 'Admin',
            'email' => 'admin@yahoo.com',
            'password' => bcrypt(123),
            'privilege' => 1
        ]);
        User::create([
            'firstname' => 'abdu',
            'lastname' => 'shahin',
            'email' => 'abdu@yahoo.com',
            'phone' => '01098277160',
            'password' => bcrypt(123),
            'privilege' => 0
        ]);
        User::create([
            'firstname' => 'dyaa',
            'lastname' => 'ahmed',
            'email' => 'dyaa@yahoo.com',
            'password' => bcrypt(123),
            'privilege' => 0
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFbSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('page_id');
            $table->foreign('page_id')->references('id')->on('fb_pages')->onDelete('cascade');
            $table->unsignedInteger('repeat')->default('0')->comment('0 No Repeat - 1 Repeat');
            $table->unsignedInteger('time_interval')->default('5')->comment('no less than 5 min.');
            $table->unsignedInteger('posts_count')->default('15')->comment('no more than 15 post.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_settings');
    }
}

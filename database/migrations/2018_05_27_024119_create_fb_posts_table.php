<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFbPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('post_type')->comment('0 Text - 1 Image - 2 Video');
            $table->longText('text')->nullable();
            $table->text('image')->nullable();
            $table->text('url')->nullable();
            $table->unsignedInteger('page_id');
            $table->foreign('page_id')->references('id')->on('fb_pages')->onDelete('cascade');
            $table->dateTime('post_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_posts');
    }
}

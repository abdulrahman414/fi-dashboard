<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFbMessagesLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_messages_log', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('message_id');
            $table->foreign('message_id')->references('id')->on('fb_messages')->onDelete('cascade');
            //$table->text('fb_message_id');
            $table->text('fb_account_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_messages_log');
    }
}

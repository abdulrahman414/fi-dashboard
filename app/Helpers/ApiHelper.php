<?php
namespace App\Helpers;

class ApiHelper
{
    public static function Get($dataset)
    {
        $client = new \GuzzleHttp\Client(['cookies' => true]);
        
        $res = $client->request('GET', 
        env("sap_hostname").$dataset 
        , [
            'headers' => ['x-csrf-token' => 'Fetch'],
            'auth' => [env("sap_username"), env("sap_password")]
        ]
        );
        $response = json_decode($res->getBody());
        return $response->d;
    }

    public static function JsonPost($dataset,$recoreds)
    {
        $client = new \GuzzleHttp\Client(['cookies' => true]);
        
        $res = $client->request('GET', 
        'http://41.39.134.67:50000/sap/opu/odata/SAP/ZPHOTO_SRV/' 
        , [
            'headers' => ['x-csrf-token' => 'Fetch'],
            'auth' => [env("sap_username"), env("sap_password")]
        ]
        );

        foreach ($res->getHeaders() as $name => $values) {
            if($name == "x-csrf-token")
                $token = implode(', ', $values);
            //echo $name . ': ' . implode(', ', $values) . "<br/>";
        }

        
        $jar = new \GuzzleHttp\Cookie\CookieJar();
        $cookieJar = $client->getConfig('cookies');
   

        $response = $client->post(env("sap_hostname").$dataset , [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'X-XHR-Logon' => 'accept="iframe"',
                'MaxDataServiceVersion' => '2.0',
                'Accept-Encoding' => 'gzip, deflate',
                'Accept-Language' => 'en',
                'Host' => '41.39.134.67:5000',
                'x-csrf-token' => $token,
                'X-Requested-With' => 'XMLHttpRequest',
                'Connection' => 'keep-alive',
                'Host' => '41.39.134.67:5000'
            ],
            'cookies' => $cookieJar,
            'auth' => [env("sap_username"), env("sap_password")],
            'json' => ($recoreds)
        ]);

        return $res->getStatusCode();
    }
}
?>
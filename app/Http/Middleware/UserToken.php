<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class UserToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(count(Auth::user()->tokens))
            return $next($request);
        else
            return redirect('/home');
    }
}

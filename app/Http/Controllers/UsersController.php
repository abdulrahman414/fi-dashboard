<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Package;
use App\Subscription;
use App\Invoice;
use App\facebook\Token;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view("users.list",compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $packages = Package::all();
        return view("users.create",compact('packages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = User::create([
            "firstname" => $request->firstname,
            "lastname" => $request->lastname,
            "email" => $request->email,
            "phone" => $request->phone,
            "password" => bcrypt($request->password),
            "privilege" => 0
        ])->id;

        $invoice_id = Invoice::create([
            "user_id" => $user_id,
            "amount" => $request->amount,
            "invoice_description" => $request->invoice_description
        ])->id;
        $daterange = explode(' - ',"05/30/2018 - 06/06/2018");
        $subscription_from = date("Y-m-d",strtotime($daterange[0]));
        $subscription_to = date("Y-m-d",strtotime($daterange[1]));
        Subscription::create([
            "user_id" => $user_id,
            "from" => $subscription_from,
            "to" => $subscription_to,
            "invoice_id" => $invoice_id,
            "package_id" => $request->package_id
        ]);
        return json_encode(array("success"=>$user_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view("users.view",compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $packages = Package::all();
        $user = User::findOrFail($id);
        return view("users.edit",compact('packages','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        User::where("id",$id)->update([
            "firstname" => $request->firstname,
            "lastname" => $request->lastname,
            "email" => $request->email,
            "phone" => $request->phone,
            "password" => bcrypt($request->password),
            "privilege" => 0
        ]);
        return json_encode(array("success"=>true));
    }
    public function tokens($user_id)
    {
        $tokens = Token::where("user_id",$user_id)->get();
        return view("facebook.tokens.list",compact('tokens','user_id'));
    }
    public function create_token($user_id)
    {
        return view("facebook.tokens.create");
    }
    public function store_token(Request $request,$user_id)
    {
        Token::firstOrCreate([
            "user_id" => $user_id,
            "token" => $request->token
        ]);
        return redirect()->back();
    }
    public function destroy_token($id)
    {
        Token::destroy($id);
        return "true";
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return "true";
    }

    public function edit_profile()
    {
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        return view("users.edit",compact('user'));
    }
    public function update_profile(Request $request)
    {
        $id = Auth::user()->id;

        $data = [
            "firstname" => $request->firstname,
            "lastname" => $request->lastname,
            "email" => $request->email,
            "phone" => $request->phone
        ];
        if(request()->password)
            $data["password"] = bcrypt($request->password);
        if($request->image)
        {
           
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
           
            $request->image->move(public_path('images/users/'), $imageName);
            $data["image"] = $imageName;
        }
        User::where("id",$id)->update($data);
        return redirect()->back();
    }

}

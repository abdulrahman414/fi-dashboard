<?php

namespace App\Http\Controllers\facebook;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\FbHelper;
use App\Helpers\LogHelper;
use App\User;
use App\facebook\CommentLog;
use App\facebook\MessageLog;
use App\facebook\Post;
use URL;

class FacebookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $token_index=0,$user_id=0;
    private function get_users(){
        return User::where('privilege',0)->get();
    }
    private function get_all_user_pages($userId){
        return User::where('id',$userId)->with([
            'pages.page_tokens.token',
            'pages.word_groups.group.words',
            'pages.word_groups.group.comments',
            'pages.word_groups.group.messages'
            ])->first();
    }
    private function comment_action_decision($fb_comment_id){
        return CommentLog::where('fb_comment_id',$fb_comment_id)->count();
    }
    private function check_the_comments($fb_comment,$words,$comments,$messages,$api)
    {
        // Check to Skip Or Reply the Comment
        $found=$this->comment_action_decision($fb_comment->id);
                        
                        $words=array_column(($words)->toArray(),'word');
                        if(!$found)
                        {
                            if($fb_comment->message!=str_ireplace($words,"_",$fb_comment->message))
                            {
                                
                                $replies=array_column(($comments)->toArray(),'text');
                                $replies_ids=array_column(($comments)->toArray(),'id');
                                $reply_index=mt_rand(0,count($replies)-1);
                                $reply=str_replace(" ","+",$replies[$reply_index]);
                                $res=$api->PostReply($fb_comment->id,$reply);
                                if(isset($res['error'])) LogHelper::create($user->id,$token_id,$page->id,"Post Reply");
                                else{
                                    CommentLog::create([
                                        'comment_id'=> $replies_ids[$reply_index],
                                        'fb_comment_id'=> $fb_comment->id,
                                        'fb_account_id' => $fb_comment->from->id
                                    ]);

                                    $privateMessages=array_column(($messages)->toArray(),'text');
                                    $privateMessages_ids=array_column(($messages)->toArray(),'id');
                                    $message_index=mt_rand(0,count($privateMessages)-1);
                                    $message=str_replace(" ","+",$privateMessages[$message_index]);
                                    $res=$api->PostMessage($fb_comment->id,$message);
                                    if(isset($res['error'])) LogHelper::create($user->id,$token_id,$page->id,"Post Message");
                                    else {
                                        MessageLog::create([
                                        'message_id' => $privateMessages_ids[$message_index],
                                        'fb_account_id' => $fb_comment->from->id
                                        ]);
                                    }
                                }
                             }
                        }
    }
    private function choose_random_token($tokens){
       $tokens=array_column(array_column(($tokens)->toArray(),'token'),'token');
       $index=mt_rand(0,(count($tokens)-1));
       $this->token_index=$index;
       return $tokens[$index];
    }    
    private function check_posts($posts,$page,$api){
        foreach ($posts as $post) {
            $fb_comments=$api->GetComments($post->id)->data;
            if(isset($fb_comments['error'])){ 
                //Here We Can Record in Which User page the Token Expired
                LogHelper::create($user->id,$token_id,$page->id,"Get Comments");
                continue;
               }else {
            //dd($fb_comments);
            foreach($fb_comments as $fb_comment){
                foreach($page->word_groups as $word_group){
                   $this->check_the_comments($fb_comment,$word_group->group->words,$word_group->group->comments,$word_group->group->messages,$api);
                }
            }
        }
     }
    }  
    private function check_user_pages($user,$api){
        $counter=0;
        foreach ($user->pages as $page) {
            // Get Random Token
            $api->access_token=$this->choose_random_token($page->page_tokens); 
            $posts = $api->GetPosts($page->fb_page_id);
            $token_id=$page->page_tokens[$this->token_index]->id;
           if(isset($posts['error'])){ 
            //Here We Can Record in Which User page the Token Expired
            LogHelper::create($user->id,$token_id,$page->id,"Get Posts");
            
            
            continue;
           }else {
            $posts=$posts->data;
            // Check All Posts
            $this->check_posts($posts,$page,$api);
           }
        }
    }
    public function check_comments()
    {
        $api = new FbHelper();
        //Wroked Token
        //$api->access_token="EAABu2IFZCq3oBAL3HXlXxOFJ0gZB16nC0nycczUsmZBZCWjOcxjsznl1xtRUmkh3AFuEhMuZAJcO6ZBVCm9JEqkmyu34wDzEltxpXzZCfIZBKtEJOgfBhNeZBoJ0dv2g15MhCvXBB7xLVSGB1EyiQhuC6bJdeshPNDfroRbSgJPeE4pWQf5nOhZCCPC3Qi1LFRvRMZD";
        //Expired Token
        //$api->access_token = "EAABu2IFZCq3oBAJJpWRMkhwytSSJSOMPZBQtpxY7q0lyeKJJ17xOjg3OT59ZBemVIpYdANBN6pfC4lMZCZBwZBe0SGxyxU1zn95lV4qgLNhOBvF2OlvL4uJp5n87qjkNMpREcmWunBZAT9PG15V42YCuapiIflZCAsmHWXbJaYtXwZDZD";
        
        //Get All Users
        $users=$this->get_users();
        foreach($users as $user){
            // Get All User Pages and Words Groups
            $user_data = $this->get_all_user_pages($user->id);
            // Check All User Pages
           $this->check_user_pages($user_data,$api);
        }
      
    }
    public function schedule_posts()
    {
        $api = new FbHelper();
        $users=$this->get_users();
        foreach($users as $user){
            // Get All User Pages and Words Groups
            $user_data = $this->get_all_user_pages($user->id);
            // Check All User Pages
            foreach ($user->pages as $page) {
                // Get Random Token
                //$api->access_token= "EAABu2IFZCq3oBAL3HXlXxOFJ0gZB16nC0nycczUsmZBZCWjOcxjsznl1xtRUmkh3AFuEhMuZAJcO6ZBVCm9JEqkmyu34wDzEltxpXzZCfIZBKtEJOgfBhNeZBoJ0dv2g15MhCvXBB7xLVSGB1EyiQhuC6bJdeshPNDfroRbSgJPeE4pWQf5nOhZCCPC3Qi1LFRvRMZD"; 
                $api->access_token=$this->choose_random_token($page->page_tokens);
                $token_id=$page->page_tokens[$this->token_index]->id;
                $post = Post::where("page_id",$page->id)->first();
                //$post->image = URL::to('/')."/fb/posts/".$post->image;
                $post->image = "https://scontent-mxp1-1.xx.fbcdn.net/v/t1.0-9/35543456_1734285629991675_6876048448244678656_n.jpg?_nc_cat=0&oh=61ad8cc007e9a87bf8d7c64f0f12fbfd&oe=5BC03D26";
                $res= $this->post_to_page($page->page_id,$post,$api);
                if(isset($res['error'])) 
                {
                    LogHelper::create($user->id,$token_id,$page->id,"Post To Page");
                }
            }
        }
    }
    private function post_to_page($page_id,$post,$api)
    {
        return $api->PostToPage($page_id,$post);
    }
}

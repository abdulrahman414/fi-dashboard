<?php

namespace App\Http\Controllers\facebook;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\facebook\Page;
use App\facebook\Token;
use Illuminate\Support\Facades\Auth;
use App\facebook\Settings;
use Illuminate\Support\Facades\Redirect;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $pages = Auth::user()->pages;
        /* if(count($pages)==1) */
        return view("facebook.settings.index", compact('pages'));
    }



    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function page_settings(Request $request)
    {
        $settings = Settings::where('page_id', $request->page_id)->first();
        $page_id = $request->page_id;
        return view("facebook.settings.settings", compact('settings', 'page_id'));

    }
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save_settings(Request $request)
    {
        $settings=array();
        if (empty($request->repeat)) {
            $settings = [
                "time_interval" => $request->time_interval,
                "posts_count" => $request->posts_count
            ];
        } else {
            $settings = [
                "repeat" => $request->repeat,
                "time_interval" => $request->time_interval,
                "posts_count" => $request->posts_count
            ];

        }
        $page_name=Page::findOrFail($request->page_id)->page_name;
        Settings::where('page_id', $request->page_id)->update($settings);
        return redirect()->route('fb_settings')->with('success', "$page_name settings has been updated successfully!");
    }


}

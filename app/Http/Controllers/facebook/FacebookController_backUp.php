<?php

namespace App\Http\Controllers\facebook;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\FbHelper;
use App\User;
use App\facebook\CommentLog;
use App\facebook\MessageLog;

class FacebookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function get_users(){
        return User::where('privilege',0)->get();
    }
    public function check_comments()
    {
        $api = new FbHelper();
        $api->access_token="EAABu2IFZCq3oBAL3HXlXxOFJ0gZB16nC0nycczUsmZBZCWjOcxjsznl1xtRUmkh3AFuEhMuZAJcO6ZBVCm9JEqkmyu34wDzEltxpXzZCfIZBKtEJOgfBhNeZBoJ0dv2g15MhCvXBB7xLVSGB1EyiQhuC6bJdeshPNDfroRbSgJPeE4pWQf5nOhZCCPC3Qi1LFRvRMZD";
        //$api->access_token = "EAABu2IFZCq3oBAJJpWRMkhwytSSJSOMPZBQtpxY7q0lyeKJJ17xOjg3OT59ZBemVIpYdANBN6pfC4lMZCZBwZBe0SGxyxU1zn95lV4qgLNhOBvF2OlvL4uJp5n87qjkNMpREcmWunBZAT9PG15V42YCuapiIflZCAsmHWXbJaYtXwZDZD";
        $users=$this->get_users();
        
        $user = User::where('id',Auth::user()->id)->with([
            'pages.page_tokens.token',
            'pages.word_groups.group.words',
            'pages.word_groups.group.comments',
            'pages.word_groups.group.messages'
            ])->first();
        foreach ($user->pages as $page) {
            $tokens=array_column(array_column(($page->page_tokens)->toArray(),'token'),'token'); 
            $posts = $api->GetPosts($page->fb_page_id)->data;
            foreach ($posts as $post) {
                $fb_comments=$api->GetComments($post->id)->data;
                //dd($fb_comments);
                foreach($fb_comments as $fb_comment){
                    foreach($page->word_groups as $word_group){
                        
                        $found=CommentLog::where('fb_comment_id',$fb_comment->id)->count();
                        
                        $words=array_column(($word_group->group->words)->toArray(),'word');
                        if(!$found){
                            if($fb_comment->message!=str_ireplace($words,"_",$fb_comment->message))
                            {
                                
                                $replies=array_column(($word_group->group->comments)->toArray(),'text');
                                $replies_ids=array_column(($word_group->group->comments)->toArray(),'id');
                                $reply_index=mt_rand(0,count($replies)-1);
                                $reply=str_replace(" ","+",$replies[$reply_index]);
                                $api->PostReply($fb_comment->id,$reply);
                                CommentLog::create([
                                    'comment_id'=> $replies_ids[$reply_index],
                                    'fb_comment_id'=> $fb_comment->id,
                                    'fb_account_id' => $fb_comment->from->id
                                ]);

                                $privateMessages=array_column(($word_group->group->messages)->toArray(),'text');
                                $privateMessages_ids=array_column(($word_group->group->messages)->toArray(),'id');
                                $message_index=mt_rand(0,count($privateMessages)-1);
                                $message=str_replace(" ","+",$privateMessages[$message_index]);
                                $api->PostMessage($fb_comment->id,$message);
                                MessageLog::create([
                                    'message_id' => $privateMessages_ids[$message_index],
                                    'fb_account_id' => $fb_comment->from->id
                                ]);
                            }
                        }
                    }
                }
            }

        }
    }
    private function pages()
    {

        return Auth::user()->pages;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\facebook;

use Illuminate\Http\Request;
use App\facebook\PageWordGroups;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\facebook\WordGroup;

class PageWordGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $wordGroups= User::where('id',Auth::user()->id)->with('pages.word_groups.group')->get();
        return view("facebook.page_word_group.list",compact('wordGroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pages(){
        $pages = Auth::user()->pages;
        return view('facebook.page_word_group.pages',compact('pages'));
    }
    public function create(Request $request)
    {
        $selectedWordGroups=PageWordGroups::where('page_id',$request->page_id)->get();
        $wordGroups=Auth::user()->word_groups;
        $id=$request->page_id;
        return view("facebook.page_word_group.create",compact('selectedWordGroups','wordGroups','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->delete($request->page_id);
        foreach($request->words_groups as $group_id){
            PageWordGroups::create([
                'group_id'=> $group_id,
                'page_id' => $request->page_id
            ]);

        }
        return redirect()->route('all_pages')->with('success','Your Word Groups has been added successfully to your page');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->delete($id);
        return redirect()->back()->with('success','Your Page Word Groups has been deleted successfully');
    }
    private function delete($id){
        PageWordGroups::where("page_id",$id)->delete();
    }
}

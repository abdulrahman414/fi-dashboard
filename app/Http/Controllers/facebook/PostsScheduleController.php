<?php

namespace App\Http\Controllers\facebook;

use Illuminate\Http\Request;
use App\facebook\PageWordGroups;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\facebook\Post;

class PostsScheduleController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view("facebook.posts_schedule.list",compact("posts"));
    }
    public function create()
    {
        $pages = Auth::user()->pages;
        return view("facebook.posts_schedule.create",compact('pages'));
    }
    public function store(Request $request)
    {
        
        if($request->image){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('fb/posts'), $imageName);
        }else{
            $imageName = "";
        }
        Post::create([
            "post_type" => $request->post_type,
            "text" => $request->text,
            "image" => $imageName,
            "page_id" => $request->page_id
        ]);
        return back()->with('success','Your Post has been added successfully');
    }
    public function edit($id)
    {
        $pages = Auth::user()->pages;
        $post = Post::findOrfail($id);
        return view("facebook.posts_schedule.edit",compact('pages','post'));
    }
    public function update(Request $request)
    {
        $post_id = $request->post_id;

        $data = [
            "post_type" => $request->post_type,
            "text" => $request->text,
            "page_id" => $request->page_id
        ];
        if($request->image){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('fb/posts'), $imageName);
            $data["image"] = $imageName;
        }
        Post::where('id',$post_id)->update($data);
        return back()->with('success','Your Post has been added successfully');
    }
    public function destroy($id)
    {
        Post::destroy($id);
        return "true";
    }
}
?>
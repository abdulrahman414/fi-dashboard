<?php

namespace App\Http\Controllers\facebook;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\facebook\WordGroup;
use App\facebook\Word;
use App\facebook\Comment;
use App\facebook\Message;

class WordGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $wordGroups= WordGroup::where('user_id',Auth::user()->id)->with(['words','comments','messages'])->get();
       return view("facebook.word_group.list",compact('wordGroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("facebook.word_group.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $words=explode(",",$request->words);
        $comments=explode(",",$request->comments);
        $messages=explode(",",$request->messages);
        $group=WordGroup::create([
            "name"=>$request->name,
            "user_id"=>Auth::user()->id
        ]);
        $this->add_words($words,$group);
        $this->add_comments($comments,$group);
        $this->add_messages($messages,$group);
        return redirect()->back()->with("success","Your Group Template has been added successfully !");
    }
    private function add_words($words ,$group){
        foreach($words as $word)
        {
            Word::create([
                "word" => $word,
                "group_id"=>$group->id
            ]);
        }
    }
    private function add_comments($comments ,$group){
        foreach($comments as $comment)
        {
            Comment::create([
                "text" => $comment,
                "group_id"=>$group->id
            ]);
        }
    }
    private function add_messages($messages ,$group){
        foreach($messages as $message)
        {
            Message::create([
                "text" => $message,
                "group_id"=>$group->id
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Word::where("group_id",$id)->delete();
        Comment::where("group_id",$id)->delete();
        Message::where("group_id",$id)->delete();
        WordGroup::destroy($id);
        return redirect()->back()->with('success','Word Group has been deleted successfully !');
    }
}

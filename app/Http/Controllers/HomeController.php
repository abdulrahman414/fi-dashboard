<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ApiHelper;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    public function OpenItems($IvBukrs,$type)
    {
        /*

        blart
        konto
        belnr
        value = dmshb
        curr = hwaer
        */
        //$IvBukrs = "0005";
        //$type = "D";
        $data = ApiHelper::Get('Z_FI_ODATA_SRV/open_itemsSet?$filter=IvBukrs eq \''.$IvBukrs.'\' and IvKoart eq \''.$type.'\'&$format=json');
        //return response()->json($data);
        $result = array();
        foreach ($data->results as $key => $value) {

            if(date("Y",(int)substr(preg_replace('/[^0-9]/', '', $value->Faedt), 0, -3)) != "2018") continue;
            $result['subvalues'][] = [
                "name" => $value->Belnr,
                "shkzg" => $value->Shkzg,
                "blart" => $value->Blart,
                "konto" => $value->Konto,
                "value" => abs((int)$value->Dmshb)
            ];
        }
        return response()->json($result);
    }
    public function OpenItemsIntervals($IvBukrs,$type)
    {
        $data = ApiHelper::Get('Z_FI_ODATA_SRV/open_itemsSet?$filter=IvBukrs eq \''.$IvBukrs.'\' and IvKoart eq \''.$type.'\'&$format=json');
        $result = array();
        foreach ($data->results as $key => $value) {
            if(date("Y",(int)substr(preg_replace('/[^0-9]/', '', $value->Faedt), 0, -3)) != "2018") continue;
            $date = date("Y-m-d",(int)substr(preg_replace('/[^0-9]/', '', $value->Faedt), 0, -3));
            $result[] = [
                "date" => $date,
                "days" => (int)((time()-(int)substr(preg_replace('/[^0-9]/', '', $value->Faedt), 0, -3))/60/60/24),
            ];
        }
        return response()->json($result);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}

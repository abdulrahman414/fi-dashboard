<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    //
    protected $fillable = [
        'user_id', 'from','to','invoice_id','package_id'
    ];
    public function invoice(){
        return $this->hasOne(Invoice::class,'id','invoice_id');
    }
    public function package(){
        return $this->belongsTo(Package::class,'package_id','id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    protected $fillable = [
        'name', 'description', 'logo','page_count','site_id'
    ];
    public function site(){
        return $this->belongsTo(Site::class,'site_id','id');
    }
    
}

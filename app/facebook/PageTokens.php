<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class PageTokens extends Model
{
    //
    protected $fillable = [
        'token_id', 'page_id'
    ];
    protected $table='fb_page_tokens';
    public function token(){
        return $this->belongsTo(Token::class,'token_id','id');
    }
}

<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class PostLog extends Model
{
    //
    protected $fillable = [
        'post_id', 'fb_post_id'
    ];
    protected $table='fb_posts_log';
    public function post(){
        return $this->belongsTo(Post::class,'post_id','id');
    }
}

<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable = [
        'group_id', 'text', 'fb_post_id'
    ];
    protected $table='fb_comments';
}

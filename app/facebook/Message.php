<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $fillable = [
        'group_id', 'text', 'fb_post_id'
    ];
    protected $table='fb_messages';
}

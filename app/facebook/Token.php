<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    //
    protected $fillable = [
        'token', 'user_id'
    ];
    protected $table='fb_tokens';
    public function page(){
        return $this->hasOne(PageTokens::class,'token_id','id');
    }
}

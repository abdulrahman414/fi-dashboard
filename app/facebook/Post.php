<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $fillable = [
        'post_type', 'text','image','url','page_id','post_time'
    ];
    protected $table='fb_posts'; 
    public function page(){
        return $this->belongsTo(Page::class,'page_id','id');
    }

    public function getPostTypeNameAttribute()
    {
        switch ($this->post_type) {
            case '0':
                return "Text";
                break;
            case '1':
                return "Photo";
                break;
            case '2':
                return "Video";
                break;
        }
    }
}

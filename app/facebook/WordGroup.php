<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class WordGroup extends Model
{
    //
    protected $fillable = [
        'name','user_id'
    ];
    protected $table='fb_word_group';
    public function words(){
        return $this->hasMany(Word::class,'group_id','id');
    }
    public function comments(){
        return $this->hasMany(Comment::class,'group_id','id');
    }
    public function messages(){
        return $this->hasMany(Message::class,'group_id','id');
    }
}

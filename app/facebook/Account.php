<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    protected $fillable = [
        'fb_id', 'user_name', 'name', 'email','phone','birthdate'
    ];
    protected $table='fb_accounts';
}

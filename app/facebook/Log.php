<?php

namespace App\facebook;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    //
    protected $fillable = [
        'user_id', 'token_id','page_id', 'action','status'
    ];
    public function token(){
        return $this->belongsTo(Token::class,'token_id','id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function page(){
        return $this->belongsTo(Page::class,'page_id','id');
    }
}

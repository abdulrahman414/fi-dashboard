<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //
    protected $fillable = [
        'fb_page_id', 'page_name','user_id'
    ];
    protected $table='fb_pages';

    public function page_tokens(){
        return $this->hasMany(PageTokens::class,'page_id','id');
    }
    public function posts(){
        return $this->hasMany(Post::class,'page_id','id');
    }
    public function word_groups(){
        return $this->hasMany(PageWordGroups::class,'page_id','id');
    }
}

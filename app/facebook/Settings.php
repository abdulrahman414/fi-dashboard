<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    protected $fillable = [
        'repeat', 'time_interval','page_id','posts_count'
    ];
    protected $table='fb_settings';
}

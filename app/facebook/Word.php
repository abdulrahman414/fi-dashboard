<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    //
    protected $fillable = [
        'word', 'group_id'
    ];
    protected $table='fb_words';
}

<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class CommentLog extends Model
{
    //
    protected $fillable = [
        'comment_id', 'fb_comment_id', 'fb_account_id'
    ];
    protected $table='fb_comments_log';
    public function comment(){
        return $this->belongsTo(Comment::class,'comment_id','id');
    }
}

<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class MessageLog extends Model
{
    //
    protected $fillable = [
        'message_id', 'fb_message_id', 'fb_account_id'
    ];
    protected $table='fb_messages_log';
    public function message(){
        return $this->belongsTo(Message::class,'message_id','id');
    }
}

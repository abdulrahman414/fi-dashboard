<?php

namespace App\facebook;

use Illuminate\Database\Eloquent\Model;

class PageWordGroups extends Model
{
    //
    protected $fillable = [
        'group_id','page_id'
    ];
    protected $table='fb_page_word_groups';
    public function group(){
        return $this->belongsTo(WordGroup::class,'group_id','id');
    }
   
    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    //
    protected $fillable = [
        'name', 'url','logo'
    ];
    public function packages(){
        return $this->hasMany(Package::class,'site_id','id');
    }
}

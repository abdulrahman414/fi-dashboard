<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\facebook\Page;
use App\facebook\WordGroup;
use App\facebook\Token;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname', 'email', 'phone', 'password', 'privilege',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',/*  'remember_token', */
    ];

    public function subscriptions(){
        return $this->hasMany(Subscription::class,'user_id','id');
    }
    public function invoices(){
        return $this->hasMany(Invoice::class,'user_id','id');
    }
    public function fb_tokens(){
        return $this->hasMany(Token::class,'user_id','id');
    }
    public function pages(){
        return $this->hasMany(Page::class,'user_id','id');
    }
    public function word_groups(){
        return $this->hasMany(WordGroup::class,'user_id','id');
    }
    public function tokens(){
        return $this->hasMany(Token::class,'user_id','id');
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\facebook\FacebookController;

class FacebookCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FacebookCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

   
        $Facebook = new FacebookController();
        $return=$Facebook->check_comments();

        $this->info("FaceBook Command : ".date("d-m-Y H:i:s")."\n".$return);
    

    }
}
